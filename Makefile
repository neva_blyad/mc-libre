#    Makefile
#    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                            <neva_blyad@lovecri.es>
#
#    This file is part of mc-libre.
#
#    mc-libre is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    mc-libre is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with mc-libre.  If not, see <https://www.gnu.org/licenses/>.

.ONESHELL:
.PHONY: all install

# Destination path

PREFIX ?= /usr/share/

all:

install:
	install -D mc-libre.ini -t '$(PREFIX)/mc/skins/'
